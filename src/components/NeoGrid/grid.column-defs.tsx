import { ColDef } from "ag-grid-community";

const formatDateToCustomFormat = (dateString: string) => {
  const options: Intl.DateTimeFormatOptions = { year: 'numeric', month: 'long', day: 'numeric' };
  const date = new Date(dateString);
  return date.toLocaleDateString(undefined, options);
}

const phaValueFormat = (value: string) => {
  switch (value) {
    case 'Y':
      return 'Yes';
    
    case 'N':
      return 'No';
    default:
      return '';
  }
}

export const columnDefs: ColDef[] = [
  { field: "designation", headerName: "Designation", filter: true },
  { field: "discovery_date", headerName: "Discovery Date", valueFormatter: params => {
    return formatDateToCustomFormat(params.value);
    
    
  } },
  { field: "h_mag", headerName: "H (mag)", filter: "agNumberColumnFilter" },
  { field: "moid_au", headerName: "MOID (au)", filter: "agNumberColumnFilter" },
  { field: "q_au_1", headerName: "q (au)", filter: "agNumberColumnFilter" },
  { field: "q_au_2", headerName: "Q (au)", filter: "agNumberColumnFilter" },
  { field: "period_yr", headerName: "Period (yr)", filter: "agNumberColumnFilter" },
  { field: "i_deg", headerName: "Inclination (deg)", filter: "agNumberColumnFilter" },
  { field: "pha", headerName: "Potentially Hazardous", filter: true, valueFormatter: params => {
    return phaValueFormat(params.value);
  } },
  { field: "orbit_class", headerName: "Orbit Class", filter: true },
];