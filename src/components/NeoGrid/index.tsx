import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-alpine.css";

import data from "../../near-earth-asteroids.json";
import { useRef } from "react";
import classes from './grid.module.css';
import { columnDefs } from "./grid.column-defs";

const NeoGrid = (): JSX.Element => {
  const gridRef = useRef<AgGridReact | null>(null);

  const clearFilters = () => {
    gridRef.current?.api.setFilterModel(null);
    gridRef.current?.columnApi.applyColumnState({
      defaultState: {
        sort: null,
      }
    })
  }

  return (
    <div className={`ag-theme-alpine ${classes.container}`}>
      <div className={classes.pageHeader}>
        <h1>Near-Earth Object Overview</h1>
        <button onClick={clearFilters}>Clear Filters and Sorters</button>
      </div>
      <AgGridReact
        ref={gridRef}
        defaultColDef={{
          sortable: true
        }}
        rowData={data}
        columnDefs={columnDefs}
        rowGroupPanelShow={'always'}
        rowSelection={'multiple'}
        />
    </div>
  );
};

export default NeoGrid;
